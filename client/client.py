import os
import requests
import time
import socket

print(socket.gethostname())

lst = os.popen('ps -U root -u root -N').read()
lst = lst.strip("\n").split('\n')

all = []
for i in range(2,len(lst)):
    all.append(lst[i].split(" ")[-1])

procs = list(set(all))

lst = os.popen('wmctrl -l').read()
lst = lst.strip("\n").split('\n')

all = []
for i in range(2,len(lst)):
    all.append(lst[i].split(str(socket.gethostname()) + " ")[-1])

wins = list(set(all))

r = requests.post("http://shmop.liamsc.com/api/add", data={'time': time.time(), "name": str(socket.gethostname()), "procs": "\n".join(procs), "wins": "\n".join(wins)})
