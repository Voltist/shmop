from flask import Flask, request
from flask_restful import Resource, Api
from json import dumps
from flask_cors import CORS
import time
import urllib
from pymongo import MongoClient
import requests
import datetime
import random
import sys, os

# Setup flask server
app = Flask(__name__)
CORS(app)
api = Api(app)

# Connect to db
authstr = 'mongodb://%s:%s@localhost:27017/shmop' % ("admin", os.environ['shmop_db_pass'])
client = MongoClient(authstr)
db = client.shmop


# Add an entry
class main(Resource):
    def post(self):
        data = request.form.to_dict()

        collection = db[str(data["name"])]
        collection.insert_one({"time": float(data["time"]), "procs": data["procs"].split("\n"), "wins": data["wins"].split("\n")})
        return {"success": True}


# Get entries
class get(Resource):
    def get(self):
        data = request.form.to_dict()

        collection = db[str(data["name"])]

        l = list(collection.find({"time": {"$gt": int(data["min"]), "$ls": int(data["max"])}}))

        out = []
        for i in l:
            del l["_id"]
            out.append(l)

        return {"success": True, "data": out}


api.add_resource(main, '/api/add')
api.add_resource(get, '/api/get')
if __name__ == '__main__':
     app.run(port='5050')
